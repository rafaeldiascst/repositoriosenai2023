const {createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            error: false,
            sucesso: false,
            userAdmin: false,
        }
    },//Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                // alert("Dentro do setTimeout!");
                if((this.username === "Adriano" && this.password === "12342023") ||
                (this.username === "Julia" && this.password === "12345678") ||
            (this.username === "Admin" && this.password === "admin12345")){
                    alert("Login efetuado com sucesso!");
                    this.error = false;
                    this.sucesso = true;
                    if(this.username === "Admin"){
                        this.userAdmin = true;
                    }
                }
                else{
                    // alert("Login não efetuado!");
                    this.error = "Nome ou senha incorretos!";
                }
            }, 1000);
            // alert("Saiu do setTimeout!");
        },//Fechamento login
        
        paginaCadastro(){
            if(this.userAdmin ==true){

                window.location.href = "paginaCadastro.html";
                alert("ADM ok!");
            }
            else{
                alert("Faça login com usuário administrativo!");
            }
        }//Fechamento paginaCadastro

        adicionarUsuario(){
            window.location.href = "index.html";
        },//Fechamento adicionarUsuario
    },//Fechamento methods

}).mount("#app");