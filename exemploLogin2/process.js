const {createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            error: null,
            sucesso: null,
            userAdmin: false,

            //Variáveis para armazenamento das entradas do formulário da página do cadastro
            newUsername: "",
            newPassword: "",
            confirmPassword: "",

            //Declaração dos arrays para armazenamento de usuarios e senhas
            usuarios: ["admin", "adriano", "julia"],
            senhas:["1234", "1234", "1234"],
            mostrarEntrada: false,
            mostrarLista: false,
            
        }
    },//Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                // alert("Dentro do setTimeout!");
                if((this.username === "Adriano" && this.password === "12342023") ||
                (this.username === "Julia" && this.password === "12345678") ||
                (this.username === "Admin" && this.password === "admin12345")){
                    this.sucesso = "Login efetuado com sucesso!";
                    this.error = null;
                    if(this.username === "Admin"){
                        this.userAdmin = true;
                    }
                }
                else{
                    // alert("Login não efetuado!");
                    this.error = "Nome ou senha incorretos!";
                    this.sucesso = null;
                }
            },1000);
            // alert("Saiu do setTimeout!!!");
        },//Fechamento Login

        login2(){
            this.mostrarEntrada = false;

            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }
            
            setTimeout(() => {
                this.mostrarEntrada = true;

                const index = this.usuarios.indexOf(this.username);
               

                if(index !== -1 && this.senhas[index] === this.password){
                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);
                    
                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso";

                    if(this.username === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!!!";

                    }
                }//Fechamento if
                else{
                    this.error = "Nome ou senha incorretos!"; 
                    this.sucesso = null;
                }//Fechamento else

                this.username = "";
                this.password = "";
                
            }, 500);          
            

        },//Fechamento login2
        
        paginaCadastro(){            
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.sucesso = "Login ADM identificado!";
                this.error = null;
                this.mostrarEntrada = true;

                // localStorage.setItem("username", this.username);
                // localStorage.setItem("password", this.password);

                setTimeout(() => {}, 2000);

                setTimeout(() => {
                    window.location.href = "paginaCadastro.html"; 
                },1000); // Fechamento setTimeout

                
            }
            else{
                setTimeout(() => {
                    this.sucesso = null;
                    this.error = "Faça login com usuário administrativo!";
                }, 500);// Fechamento setTimeout
                
            }
        },//Fechamento paginaCadastro

        adicionarUsuario(){
            this.username = localStorage.getItem("username") || "";
            this.password = localStorage.getItem("password");

            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.username === "admin"){
                    if(this.newUsername !== ""){
                        if(!this.usuarios.includes(this.newUsername)){
                            if(this.newPassword && this.newPassword === this.confirmPassword){
                                this.usuarios.push(this.newUsername);
                                this.senhas.push(this.newPassword);

                                //Armazenamento do usário e senha no LocalStorage
                                localStorage.setItem("usuarios", JSON.stringify(this.usuarios))
                                localStorage.setItem("senhas", JSON.stringify(this.senhas));
                                this.error = null;
                                this.sucesso = "Usuário cadastrado com sucesso!"
                            }//Fechamento do if
                            else{
                                this.sucesso = null;
                                this.error = "Por favor, confirme sua senha!!!"
                            }
                        }//Fechamento if includes
                        else{
                            this.sucesso = null;
                            this.error = "O usuário informado já está cadastrado!"
                        }
                    }//Fechamento if !== ""
                    else{
                        this.error = "Por favor, digite um nome de usuário";
                        this.sucesso = null;
                    }
   
                }//Fechamento if
                else{
                    this.error = "Usuário NÃO ADM!!!";
                    this.sucesso = null;
                }//Fechamento else

                this.newUsername = "";
                this.newPassword = "";
                this.confirmPassword = "";


            }, 500);//Fechamento setTimeout
        }, //Fechamento adicionarUsuario

        verCadastrados(){            
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }
            this.mostrarLista = !this.mostrarLista;
        },//Fechamento verCadastrados

        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                //Impedir a exclusão do usuário admin
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "O usuário ADMIN não pode ser excluído!!!"                    
                }, 500);
                return;//Força a finalização do bloco / função
            }//Fechamento if usuario

            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);

                    //Atualização dos vetores no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                }//Fechamento index
            }

        },//Fechamento excluirUsuarios
    },//Fechamento methods       

}).mount("#app");